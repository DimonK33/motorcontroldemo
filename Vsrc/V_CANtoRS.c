/*!
    Copyright 2017 	�� "�����" � ��� "��� ������"

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 \file      v_CANtoRS.c
 \brief     ��������������� ������� CAN � RS � �������. �������� 
��������� � ��������� CANOpen
 \author    ��� "��� ������". http://motorcontrol.ru
 \version   v 2.0 25/03/2016

 \addtogroup v_CANtoRS
@{
*/
#include "DSP.h"
#include "main.h"



//! \memberof TCANtoRS
void CANtoRS_init(TCANtoRS *p) {

#ifdef  HW_NIIET_BOARD_SIMULATOR
//UART TX - ����� ���������������� ������ XP6 ��� 57 (������������� ����������� UART_TXD1 � ����������� �� �������� �����)
//UART RX - ����� ���������������� ������ XP6 ��� 59 (������������� ����������� UART_RXD1 � ����������� �� �������� �����)
        NT_COMMON_REG->GPIOPCTLA_bit.PIN3 = 1;   //UART_TxD1]
        NT_COMMON_REG->GPIOPCTLA_bit.PIN4 = 1;  //UART_RxD[1]
        NT_GPIOA->ALTFUNCSET = (1 << 3);
        NT_GPIOA->ALTFUNCSET = (1 << 4);

        NT_COMMON_REG->GPIOPCTLA_bit.PIN3 = 0;
        NT_COMMON_REG->GPIOPCTLA_bit.PIN4 = 0;

        p->NT_UART=NT_UART1;//������������ � �������� ����� UART (1,2,3)
#else

        // ������� � ��������� ������������ �������
        NT_COMMON_REG->GPIOPCTLD_bit.PIN1 = 1;   //UART_TxD[3]
        NT_COMMON_REG->GPIOPCTLD_bit.PIN2 = 1;  //UART_RxD[3]
        NT_GPIOD->ALTFUNCSET = (1 << 1) + (1 << 2);

        // �������� ������������ ������� F12 � F13, ����� ��� �� ������������� UART3
        NT_COMMON_REG->GPIOPCTLF_bit.PIN12 = 1;
        NT_COMMON_REG->GPIOPCTLF_bit.PIN13 = 1;

        p->NT_UART=NT_UART3;//������������ � �������� ����� UART (1,2,3)

        p->NT_UART->CR_bit.UARTEN = 1; // ��������� ������ UART
#endif


	p->NT_UART->CR_bit.UARTEN = 1; // ��������� ������ UART

	// ��������� ������� � ���� ���������:
	// ������� �������� ������� ��� ��������� BaudRate
	// �.�. ���� UART ����������� � �������� 25 ��� (��� ��������� � ����� ����������������),
	// �� ��� ��������� ������� 57600 ���/� ���������
	// �������� 25000000 / (16 * 57600) = 27,126736111111111111111111111111
	// ����� ����� I = 27
	// ������� ����� F = (int)( 0.126736111*64 + 0.5 ) = 8
	// ������� ��. � ������������
	p->NT_UART->IBRD_bit.BAUD_DIVINT= 27;
	p->NT_UART->FBRD_bit.BAUD_DIVFRAC = 8;



	p->NT_UART->LCR_H_bit.SPS = 0;		// ��� �������� ��������
	p->NT_UART->LCR_H_bit.WLEN = 3;		// ����� ������� 8 ���
	p->NT_UART->LCR_H_bit.FEN = 1;		// ������������ FIFO
	p->NT_UART->CR_bit.TXE = 1;			// ��������� ����
	p->NT_UART->CR_bit.RXE = 1;			// ��������� ��������
	p->NT_UART->LCR_H_bit.STP2 = 0;		// 1 ����-���

    p->CounterWrongCRC=0;
    p->CounterRes=0;
    p->CounterSended=0;
    p->PacketInWait=0;

}



//! �� ��������� ��� �������� ������� ������� ���������
//!API �����, � ����� �������� �������
//!���������� ��������

 //! \memberof TCANtoRS
Uint16 CANtoRS_SendP(Uint16* Data, int16 len, TCANtoRS *p) {
    volatile int16 i;
    Uint16 MyCRC;
    p->buf_out[0]=0x7E;
    for (i=0;i<len;i++)
        p->buf_out[i+1]=Data[i];
    MyCRC=CANtoRS_C_CRC(p,Data,len);
    p->buf_out[len+1]=MyCRC & 0xFF;//������ ����
    p->buf_out[len+2]=(MyCRC>>8) & 0xFF;//������ ����
    p->all_len=1+len+2;//������ �����=���������+������ + ����������� �����

    i = 0;
    // ���� ����� TX �� ��������, �� ���������� ��� ����� �� ������
    while ((p->NT_UART->FR_bit.TXFF != 1) && (i < p->all_len)){
    	p->NT_UART->DR = p->buf_out[i];
    	i++;
    }


    return 1;
}



#define CRC_MAGIC_NUMBER 0xA001
//! ������� ������� ����������� ����� ������ 
//! � ���������� � ��������. ���� ��� �����, �� ���������� 1
 //! \memberof TCANtoRS
#if defined (__GNUC__)
__attribute__((section(".fastcode")))
#endif
Uint16 CANtoRS_C_CRC(TCANtoRS *p,Uint16 *Data,Uint16 len) { //�������� ����������� ����� ��������� API ������
    int16 i,j;
    Uint16 MyCalcCRC=0xFFFF;

    for (j=0;j<len;j++) {
        MyCalcCRC=MyCalcCRC^(Data[j++]&0xFF);
        for (i=0; i<16; i++)
            if (MyCalcCRC&0x01) {
                MyCalcCRC=(MyCalcCRC>>1);
                MyCalcCRC=MyCalcCRC^CRC_MAGIC_NUMBER;
            } else
                MyCalcCRC=(MyCalcCRC>>1);
    }

    return MyCalcCRC;
}



/*! ������� ����������, ����� ������ �����-�� ������,
� ������������ �� ��� ���������� CAN �����. ��������
�������������, ����� � ������. ���-� �������� � ���������
p->MSG */
 //! \memberof TCANtoRS
void CANtoRS_Analysis(TCANtoRS *p) {
    int16 i,j;
    //������� ������ ���������� � 12�� �����,
    //�� ����� ��������� ���������� API ������
    p->MSG.id=0;
    p->MSG.id=(p->ReadPackData[0] & 7)<<8; //������ 3 ���� �������� ������;
    p->MSG.id|=p->ReadPackData[1];
    p->MSG.dlc=(p->ReadPackData[0]>>4) & 0xF; //������� 4 ���� �������� ����; ����� �������
    for (i=2,j=0;i<CANTORS_READ_DATA_MAX_LEN;i++,j++)
        p->MSG.data[j]=p->ReadPackData[i];
    p->callback_go=1;
}





/*! ���������� ���������� ���������� �� SCI ������.
�������� �� �������� ����������� �������� � ��������
����� � ������� API �����. ������� ���� �������� ���������,
����� ����� � �.�. � ����� ��������� ����������� ����� �
�������� ���������� ����������� */
 //! \memberof TCANtoRS
#if defined (__GNUC__)
__attribute__((section(".fastcode")))
#endif
void CANtoRS_Receive(TCANtoRS *p) {
    Uint16 MyReadCRC_u16;
    Uint16 MyCalcCRC;
    Uint32 temp_byte;
    Uint16 repeat;
    for (repeat=0;repeat<7; repeat++) {//��������� �� ����� n ���� �� ���� ���� � �������

        switch (p->APIpacketMode) {

        case 0: { //�������� ���������


            if (p->NT_UART->FR_bit.RXFE)
            	return;
            temp_byte = p->NT_UART->DR_bit.DATA;
            if (temp_byte!=0x7e) {
                p->MessDrop3++;
                return;
            }
            p->ReadPackDataCounter=0;//������� ������
            p->APIpacketMode=1; //���������� �������, ����� ������ ������
            break;
        };
        case 1: { //�������� ������(�������� ��������) ������
            //��� ����������� ����� ���������� � API �����

            if (p->NT_UART->FR_bit.RXFE)
            	return;
            temp_byte = p->NT_UART->DR_bit.DATA;
            p->ReadPackData[p->ReadPackDataCounter++]=temp_byte;
            if (p->ReadPackDataCounter>=CANTORS_READ_DATA_MAX_LEN) {
                p->ReadPackDataCounter=0;//������� ������
                p->ReadCRCCounter=0;//������� ����������� �����
                p->APIpacketMode=2;  //������ �������, ����� ������ � �������� CRC
            }
            break;
        };

        case 2: { //��������  CRC
            //��� ����������� ����� ���������� � CRC �����

            if (p->NT_UART->FR_bit.RXFE)
            	return;
            temp_byte = p->NT_UART->DR_bit.DATA;
            p->ReadCRC[p->ReadCRCCounter++]=temp_byte;
            if (p->ReadCRCCounter>=2) {
                p->ReadCRCCounter=0;//������� ����������� �����
                MyReadCRC_u16=(p->ReadCRC[0]&0xFF)+((p->ReadCRC[1]<<8)&0xFF00);//������������ ������������ CRC � ���� ����������
                MyCalcCRC=CANtoRS_C_CRC(p,p->ReadPackData,CANTORS_READ_DATA_MAX_LEN);
                if (MyCalcCRC!=MyReadCRC_u16) {
                    //������!
                    p->CounterWrongCRC++;
                    p->APIpacketMode=0;
                    break;
                }//����������� ����� ������� � �����

                CANtoRS_Analysis(p);
                if (p->callback_go) { //���� ��������� ����, ����� callback
                    p->callback_go=0;
                    p->APIpacketMode=3;  //��������� callback
                }
                else
                    p->APIpacketMode=0;
                return;
            }
            break;
        };
        case 3: { //��������� callback
            p->CounterRes++;
            p->callback(&co2_vars, &(p->MSG));
            p->APIpacketMode=0;
            break;
        };
        default:
            return;
        }
    }
    return;
}



/*! ������ ���������� ������ ������� � ��������
CAN ������ HeartBeat. ���� �������� �� ������� (�������� ������),
�� ��������� ����������� ������� �, ������� �������, ��������
��������� ������ */
 //! \memberof TCANtoRS
void CANtoRS_HeartBeat(TCANtoRS *p) {
    TZCanMsg MSG;
    int16 i;
    MSG.id=0xE<<7;//heartbeat
    MSG.id|=*p->nodeID;//����� ����
    MSG.dlc=1;//�����
    for (i=0;i<8;MSG.data[i++]=0);//�������
    MSG.data[0]=5;//������ heartbeat
    if (!CANtoRS_Write(&MSG,p)) { //���� �� ���������� ���������
        p->HeartCounter=(CANTORS_HEART_COUNTER_MAX-3); //������ ������� ������� ��� ��� ���
        p->HeartBeatGo=1;//� ���� ����� ������� �� �������� SDO, �� ����� �����, ��� ���� ����������
    } else {
        p->HeartBeatGo=0;
    }
}


 //! \memberof TCANtoRS
Uint16 CANtoRS_WriteHelper(TZCanMsg* MSG,TCANtoRS *p) {
    if (p->HeartBeatGo) { //����� ����������� HeartBeat
        CANtoRS_HeartBeat(p);
        p->MessDrop1++;
        return 0;//��������� ������...
    }
    if (CANtoRS_Write(MSG,p))
        p->CounterSended++;
    else {
        p->MessDrop2++;

        return 0;
    }
    return 1;
}


/*! �� ��������� CAN ������ ��������� ������-�������,
��� ������� 4 ���� - �����, ����� 1 ��� ������� � 11 �������������.
����� ��������������� ������. ����� ��������������� ������� ������������*/
 //! \memberof TCANtoRS
Uint16 CANtoRS_Write(TZCanMsg* MSG,TCANtoRS *p) {
    if (!p->PacketInWait) { //��� ������ � ������
        //�������� �� ��������� �����
        p->bufMSG=*MSG;
        p->PacketInWait=1;//����, ��� � ������ ���-�� ����
        return 1;
        //���������, ��� ������ �� 1 ������� ������ �������
    } else
        return 0;
}

 //! \memberof TCANtoRS
Uint16 CANtoRS_Write_Real(TZCanMsg* MSG,TCANtoRS *p) {
    int16 i;

    p->TempData[0]=(MSG->dlc & 0xF)<<4;
    p->TempData[0]|=(MSG->id >> 8) & 7;
    p->TempData[1]=MSG->id & 0xFF;
    for (i=0;i<8;i++) p->TempData[i+2]=MSG->data[i];
    CANtoRS_SendP(p->TempData,10,p);
    return 1;
}

/*! ���������� �� ��������������� �������, ����������� 1 �������
� �������� ������� �������� HeartBeat. ������ ���������,
���� � SCI �������� �����-�� ������ � �� �����������, �������������.
 */

 //! \memberof TCANtoRS
#if defined (__GNUC__)
__attribute__((section(".fastcode")))
#endif
void CANtoRS_calc(TCANtoRS *p) {

    //���� � ������ ���� �����, ��������� �������� � ���������� �������� � �� ���� heartbeat
    if (p->PacketInWait) {
        if (!CANtoRS_Write_Real(&p->bufMSG,p))//��������� �� �������?
            p->MessDrop2++;
        p->PacketInWait=0;//������� �����
    }
    else{//���� ������ �� ������� ���, �������������� �� ���������. ����� �� � ������������ �����, �� �� ������� ��������
      CANtoRS_Receive(p);
      p->HeartCounter++; //������� ��� HeartBeat

      //�� ���� �� ��������� HeartBeat?
      if (p->HeartCounter>=(CANTORS_HEART_COUNTER_MAX-1)) {
          p->HeartCounter=0;
          CANtoRS_HeartBeat(p);
      }

    }
}



/*@}*/
